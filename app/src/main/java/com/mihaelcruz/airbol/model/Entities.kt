package com.mihaelcruz.airbol.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaVuelo")
data class Vuelo(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int,
    @ColumnInfo(name = "fecha")
    val fecha:String,
    @ColumnInfo(name = "nombre")
    val nombre:String,
    @ColumnInfo(name = "pasajero")
    val pasajero:String,
    @ColumnInfo(name = "destino")
    val destino:String
)