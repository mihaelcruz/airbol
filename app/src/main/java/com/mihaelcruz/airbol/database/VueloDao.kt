package com.mihaelcruz.airbol.database

import androidx.room.Dao
import androidx.room.Insert
import com.mihaelcruz.airbol.model.Vuelo

@Dao
interface VueloDao {
    @Insert
    fun insetar(vuelo:Vuelo)
}