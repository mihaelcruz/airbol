package com.mihaelcruz.airbol.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mihaelcruz.airbol.fragment.opcionDosFragment
import com.mihaelcruz.airbol.model.Vuelo

@Database(
    entities = [Vuelo::class],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase: RoomDatabase() {
    abstract fun vueloDao():VueloDao
    //companion object{
    private var instancia:AppDatabase? = null
    fun obtenerInstancia(context: Context):AppDatabase{
        if(instancia == null){
            instancia = Room.databaseBuilder(context,AppDatabase::class.java,"BDvuelo").build()
        }
        return instancia as AppDatabase
    }
    //}
}