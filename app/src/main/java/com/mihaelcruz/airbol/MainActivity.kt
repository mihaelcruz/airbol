package com.mihaelcruz.airbol

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.Fragment
import com.mihaelcruz.airbol.database.AppDatabase
import com.mihaelcruz.airbol.databinding.ActivityMainBinding
import com.mihaelcruz.airbol.databinding.FragmentOpcionUnoBinding
import com.mihaelcruz.airbol.fragment.opcionDosFragment
import com.mihaelcruz.airbol.fragment.opcionUnoFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var fragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //setContentView(R.layout.activity_main)
        binding.btnOpcion01.setOnClickListener{
            fragment = opcionUnoFragment()
            incrutarFragmento()
        }
        binding.btnOpcion02.setOnClickListener{
            fragment = opcionDosFragment()
            incrutarFragmento()
        }
    }
    fun incrutarFragmento(){
        fragment?.let {
            supportFragmentManager.beginTransaction().replace(R.id.contenedor,it).commit()
        }
    }
}