package com.mihaelcruz.airbol.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mihaelcruz.airbol.R
import com.mihaelcruz.airbol.database.AppDatabase
import com.mihaelcruz.airbol.databinding.ActivityMainBinding
import com.mihaelcruz.airbol.model.Vuelo
import kotlinx.android.synthetic.main.fragment_opcion_dos.*


class opcionDosFragment : Fragment() {
    private lateinit var binding: opcionDosFragment
    /*private val appDatabase:AppDatabase by lazy {
        AppDatabase.obtenerInstancia(this)
    }*/
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?
    ): View? {
        var destino2 = ""
        if(radioCochabamba.isChecked){
            destino2 = "Cochabamba"
        }
        if(radioLaPaz.isChecked){
            destino2 = "La Paz"
        }
        if(radioSantaCruz.isChecked){
            destino2 = "Sata Cruz"
        }
        binding.btnEnviar.setOnClickListener{
            var fecha = binding.calendar.toString()
            var nombre = binding.edtNombre.text.toString()
            var pasajero = binding.edtPasajero.text.toString()
            var destino = destino2
            val vuelo = Vuelo(0,fecha,nombre,pasajero,destino)

            appDatabase.vueloDao().insetar(vuelo)
        }

        return inflater.inflate(R.layout.fragment_opcion_dos, container, false)

    }

}